﻿namespace Corneola
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            using (var game = new CorneolaGame("Corneola"))
            {
                game.Run();
            }
        }
    }
}