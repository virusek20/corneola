﻿using ViruEngine.Entities;

namespace Corneola
{
    internal class EntityDefinitions
    {
        public static EntityManager.GameEntityCreationDelegate Player = (game, level) =>
        {
            var gameEntity = new GameEntity(game, level);

            return gameEntity;
        };
    }
}