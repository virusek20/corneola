﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ViruEngine;
using ViruEngine.GameStates;
using ViruEngine.Graphics.Colors;
using ViruEngine.Graphics.Textures;
using ViruEngine.GUI;
using ViruEngine.World;

namespace Corneola.GameStates
{
    public class MainState : GameState
    {
        private GameLevel _activeLevel;
        private Texture2D _backgroundTexture;
        private bool _canDock;
        private Button _cargoButton;
        private Button _dockButton;
        private Button _equipmentButton;
        private TextBox _logBox;
        private Canvas _mapCanvas;
        private Texture2D _mapGridTexture;
        private Button _statisticsButton;
        private Button _techButton;
        private Texture2D _vignetteTexture;

        public override void LoadContent(ContentManager content)
        {
            _backgroundTexture = SinglePixelTexture.GetSinglePixelTexture(Color.White);
            _vignetteTexture = ImageTexture.FromImage("vignette.png");
            _mapGridTexture = ImageTexture.FromImage("mapGrid.png");

            ContentLoaded = true;
        }

        public override void Initialize(ViruGame game)
        {
            base.Initialize(game);

            _statisticsButton = new Button(Game, this, "statisticsButton")
            {
                Position = new Vector2(85, Game.Graphics.PreferredBackBufferHeight - 80),
                OnResize = () => new Vector2(85, Game.Graphics.PreferredBackBufferHeight - 80),
                Text = "Statistics",
                OverColor = Color.LightBlue,
                CustomDraw =
                    batch =>
                    {
                        batch.Draw(_backgroundTexture,
                            new Rectangle((int) (_statisticsButton.Position.X - 10),
                                (int) (_statisticsButton.Position.Y - 10),
                                (int) (Game.BasicFont.MeasureString(_statisticsButton.Text).X + 20),
                                (int) (Game.BasicFont.MeasureString(_statisticsButton.Text).Y + 20)), Color.DimGray);
                    },
                OnClick = () => { }
            };

            _equipmentButton = new Button(Game, this, "equipmentButton")
            {
                Position = new Vector2(185, Game.Graphics.PreferredBackBufferHeight - 80),
                OnResize = () => new Vector2(185, Game.Graphics.PreferredBackBufferHeight - 80),
                Text = "Equipment",
                OverColor = Color.LightBlue,
                CustomDraw =
                    batch =>
                    {
                        batch.Draw(_backgroundTexture,
                            new Rectangle((int) (_equipmentButton.Position.X - 10),
                                (int) (_equipmentButton.Position.Y - 10),
                                (int) (Game.BasicFont.MeasureString(_equipmentButton.Text).X + 20),
                                (int) (Game.BasicFont.MeasureString(_equipmentButton.Text).Y + 20)), Color.DimGray);
                    },
                OnClick = () => { }
            };

            _techButton = new Button(Game, this, "techButton")
            {
                Position = new Vector2(300, Game.Graphics.PreferredBackBufferHeight - 80),
                OnResize = () => new Vector2(300, Game.Graphics.PreferredBackBufferHeight - 80),
                Text = "Tech",
                OverColor = Color.LightBlue,
                CustomDraw =
                    batch =>
                    {
                        batch.Draw(_backgroundTexture,
                            new Rectangle((int) (_techButton.Position.X - 10), (int) (_techButton.Position.Y - 10),
                                (int) (Game.BasicFont.MeasureString(_techButton.Text).X + 20),
                                (int) (Game.BasicFont.MeasureString(_techButton.Text).Y + 20)), Color.DimGray);
                    },
                OnClick = () =>
                {
                    _canDock = true;
                    _logBox.WriteLine("<Docking is now avaiable>", Color.Gray);
                }
            };

            _cargoButton = new Button(Game, this, "cargoButton")
            {
                Position = new Vector2(368, Game.Graphics.PreferredBackBufferHeight - 80),
                OnResize = () => new Vector2(368, Game.Graphics.PreferredBackBufferHeight - 80),
                Text = "Cargo Bay",
                OverColor = Color.LightBlue,
                CustomDraw =
                    batch =>
                    {
                        batch.Draw(_backgroundTexture,
                            new Rectangle((int) (_cargoButton.Position.X - 10), (int) (_cargoButton.Position.Y - 10),
                                (int) (Game.BasicFont.MeasureString(_cargoButton.Text).X + 20),
                                (int) (Game.BasicFont.MeasureString(_cargoButton.Text).Y + 20)), Color.DimGray);
                    }
            };

            _dockButton = new Button(Game, this, "dockButton")
            {
                Position = new Vector2(510, Game.Graphics.PreferredBackBufferHeight + 11),
                OnResize = () => new Vector2(510, Game.Graphics.PreferredBackBufferHeight + 11),
                Text = "Dock",
                OverColor = Color.LightBlue,
                OnClick = () =>
                {
                    _canDock = false;
                    _logBox.WriteLine("<Docking...>", Color.Gray);
                    _logBox.WriteLine("<Undocking...>", Color.Gray);
                },
                CustomDraw =
                    batch =>
                    {
                        batch.Draw(_backgroundTexture,
                            new Rectangle((int) (_dockButton.Position.X - 10), (int) (_dockButton.Position.Y - 10),
                                (int) (Game.BasicFont.MeasureString(_dockButton.Text).X + 20),
                                (int) (Game.BasicFont.MeasureString(_dockButton.Text).Y + 20)), Color.DimGray);
                    },
                CustomUpdate = (time, point, click) =>
                {
                    if (_canDock)
                    {
                        if (_dockButton.Position.Y == Game.Graphics.PreferredBackBufferHeight - 80) return;
                        _dockButton.Position = new Vector2(_dockButton.Position.X, _dockButton.Position.Y - 1);
                    }
                    else
                    {
                        if (_dockButton.Position.Y == Game.Graphics.PreferredBackBufferHeight + 11) return;
                        _dockButton.Position = new Vector2(_dockButton.Position.X, _dockButton.Position.Y + 1);
                    }
                }
            };

            _mapCanvas = new Canvas(game, this, "mapCanvas")
            {
                Position = new Vector2(75, 75),
                OnResize = () => new Vector2(75, 75),
                CustomDraw = batch =>
                {
                    batch.DrawString(Game.BasicFont, "SIK CANVAS YO", new Vector2(10, 10),
                        ColorRandomizer.GetRandomColor());
                    batch.Draw(_mapGridTexture,
                        new Rectangle((int) -_mapCanvas.CameraPosition.X, (int) -_mapCanvas.CameraPosition.Y,
                            (int) _mapCanvas.Size.X, (int) _mapCanvas.Size.Y),
                        new Rectangle((int) -_mapCanvas.CameraPosition.X, (int) -_mapCanvas.CameraPosition.Y,
                            (int) _mapCanvas.Size.X, (int) _mapCanvas.Size.Y), Color.White);
                    batch.Draw(_vignetteTexture,
                        new Rectangle((int) -_mapCanvas.CameraPosition.X, (int) -_mapCanvas.CameraPosition.Y,
                            (int) _mapCanvas.Size.X, (int) _mapCanvas.Size.Y), Color.White);
                },
                Size =
                    new Vector2(Game.Graphics.PreferredBackBufferWidth - 175,
                        Game.Graphics.PreferredBackBufferHeight - 175),
                BorderColor = Color.Lime,
                CustomUpdate = (time, point, click) =>
                {
                    if (Game.KState.IsKeyDown(Keys.W))
                        _mapCanvas.CameraPosition = _mapCanvas.CameraPosition + new Vector2(0, -5);
                    if (Game.KState.IsKeyDown(Keys.A))
                        _mapCanvas.CameraPosition = _mapCanvas.CameraPosition + new Vector2(-5, 0);
                    if (Game.KState.IsKeyDown(Keys.S))
                        _mapCanvas.CameraPosition = _mapCanvas.CameraPosition + new Vector2(0, 5);
                    if (Game.KState.IsKeyDown(Keys.D))
                        _mapCanvas.CameraPosition = _mapCanvas.CameraPosition + new Vector2(5, 0);
                }
            };

            _logBox = new TextBox(game, this, "logTextBox")
            {
                BorderColor = Color.Gray,
                BackgroundColor = Color.Black,
                Dimensions =
                    new Rectangle(Game.Graphics.PreferredBackBufferWidth - 360,
                        Game.Graphics.PreferredBackBufferHeight - 450, 260, 350),
                OnResize =
                    () =>
                        new Vector2(Game.Graphics.PreferredBackBufferWidth - 360,
                            Game.Graphics.PreferredBackBufferHeight - 450)
            };

            _logBox.WriteLine("Auxalarian Empire defended S1_P12 against Raiders", Color.Gray);
            _logBox.WriteLine("Torontoronton's dumbiees claimed S8_P4", Color.Gray);
            _logBox.WriteLine("Drungon's weed corporation has lost S8_P4", Color.Gray);
            _logBox.WriteLine("<Generators are stabilized>", Color.Lime);
            _logBox.WriteLine("<Energy output - 20>", Color.Lime);
            _logBox.WriteLine("<Scanners detected hostile ships in close sector>", Color.SlateBlue);
            _logBox.WriteLine("<<<Battle won>>>", Color.Gold);
            _logBox.WriteLine("<Scanners don't detect any ships in close sectors>", Color.SlateBlue);
            _logBox.WriteLine("<Hull is badly damaged, temporary repairs required>", Color.Red);
            _logBox.WriteLine("", Color.Gray);
            _logBox.WriteLine("<Hull temporary repaired, visiting Docks is recommended>", Color.Lime);
            _logBox.WriteLine("", Color.Gray);
            _logBox.WriteLine("Drungon's weed corporation claimed S8_4", Color.Gray);
            _logBox.WriteLine("Torontoronton's dumbies has been eradicated", Color.Gray);
            _logBox.WriteLine("", Color.Gray);
            _logBox.WriteLine("<You are aproaching S2_P9>", Color.SlateBlue);
            _logBox.WriteLine("<Docking is now avaiable>", Color.Gray);
            _logBox.WriteLine("<Docking...>", Color.Gray);
            _logBox.WriteLine("<Hull is fully repaired>", Color.Lime);
            _logBox.WriteLine("<Undocking...>", Color.Gray);
            _logBox.WriteLine("", Color.Gray);
            _logBox.WriteLine("<Scanners detected a large hostile fleet in the Sector>", Color.SlateBlue);
            _logBox.WriteLine("<S2_P9 has been destroyed by Sontaran's empire>", Color.Gray);
            _logBox.WriteLine("<Hull is critically damaged, temporary repairs required>", Color.Red);

            Loaded = true;
        }

        public override void Update(GameTime gameTime)
        {
            DoCheck(gameTime, new Point(Game.MState.X, Game.MState.Y), Game.JustPressed());
        }

        public override void Draw(GameTime gameTime)
        {
            Game.GraphicsDevice.Clear(Color.Black);

            Game.SpriteBatch.Begin();

            DoDraw(gameTime, Game.SpriteBatch);

            Game.SpriteBatch.End();
        }

        public override void RecalculateSizes()
        {
            DoResize();
        }
    }
}