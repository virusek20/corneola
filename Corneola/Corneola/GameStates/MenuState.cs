﻿using Microsoft.Xna.Framework;
using ViruEngine;
using ViruEngine.GameStates;
using ViruEngine.GUI;

namespace Corneola.GameStates
{
    public class MenuState : GameState
    {
        private Button _creditsButton;
        private Label _creditsLabel;
        private YesNoDialog _exitDialog;
        private Button _gameHelpButton;
        private Label _gameplayHelpLabel;

        public override void Initialize(ViruGame game)
        {
            base.Initialize(game);

            new Button(game, this, "exitButton")
            {
                OnResize = () => new Vector2(10, game.Graphics.PreferredBackBufferHeight - 30),
                Position = new Vector2(10, Game.Graphics.PreferredBackBufferHeight - 30),
                Text = "Exit",
                OnClick = () => _exitDialog.Show(),
                OverColor = Color.DarkRed
            };

            new Button(game, this, "helpButton")
            {
                OnResize = () => new Vector2(10, game.Graphics.PreferredBackBufferHeight - 60),
                Position = new Vector2(10, Game.Graphics.PreferredBackBufferHeight - 60),
                Text = "Help",
                OnClick = () => { SetButtonVisibilities(1); },
                OverColor = Color.DarkRed
            };

            new Button(game, this, "settingsButton")
            {
                OnResize = () => new Vector2(10, game.Graphics.PreferredBackBufferHeight - 90),
                Position = new Vector2(10, Game.Graphics.PreferredBackBufferHeight - 90),
                Text = "Settings",
                OnClick = () => { SetButtonVisibilities(0); },
                OverColor = Color.DarkRed
            };

            new Button(game, this, "loadButton")
            {
                OnResize = () => new Vector2(10, game.Graphics.PreferredBackBufferHeight - 120),
                Position = new Vector2(10, Game.Graphics.PreferredBackBufferHeight - 120),
                Text = "Load Game",
                OnClick = () => { SetButtonVisibilities(0); },
                OverColor = Color.DarkRed
            };

            new Button(game, this, "newGameButton")
            {
                OnResize = () => new Vector2(10, game.Graphics.PreferredBackBufferHeight - 150),
                Position = new Vector2(10, Game.Graphics.PreferredBackBufferHeight - 150),
                Text = "New Game",
                OnClick = () =>
                {
                    ((CorneolaGame) Game).MainState = new MainState();
                    Game.ActiveState = new MainState();
                },
                OverColor = Color.DarkRed
            };

            _gameHelpButton = new Button(game, this, "gameHelpButton")
            {
                OnResize = () => new Vector2(150, game.Graphics.PreferredBackBufferHeight - 150),
                Position = new Vector2(150, game.Graphics.PreferredBackBufferHeight - 150),
                Text = "Gameplay Help",
                OverColor = Color.DarkRed,
                OnClick = () => { SetButtonVisibilities(11); },
                Visibility = HiddenState.Hidden
            };

            _creditsButton = new Button(game, this, "creditsButton")
            {
                OnResize = () => new Vector2(150, game.Graphics.PreferredBackBufferHeight - 120),
                Position = new Vector2(150, game.Graphics.PreferredBackBufferHeight - 120),
                Text = "Credits",
                OverColor = Color.DarkRed,
                OnClick = () => { SetButtonVisibilities(12); },
                Visibility = HiddenState.Hidden
            };

            _creditsLabel = new Label(game, this, "creditsLabel")
            {
                OnResize = () => new Vector2(310, game.Graphics.PreferredBackBufferHeight - 150),
                Position = new Vector2(310, game.Graphics.PreferredBackBufferHeight - 150),
                Text = "Main coder: virusek20 \nGame idea: Wheero \nArt: Wheero",
                Visibility = HiddenState.Hidden
            };

            _gameplayHelpLabel = new Label(game, this, "gameplayHelpLabel")
            {
                OnResize = () => new Vector2(310, game.Graphics.PreferredBackBufferHeight - 150),
                Position = new Vector2(310, game.Graphics.PreferredBackBufferHeight - 150),
                Text = "INSERT HELP HERE",
                Visibility = HiddenState.Hidden
            };

            _exitDialog = new YesNoDialog(game, this, "exitDialog")
            {
                YesText = "Quit",
                NoText = "Cancel",
                OnYes = () => Game.Exit(),
                OnNo = () => _exitDialog.Hide(),
                Priority = 2,
                Text = "Do you really want to quit the game?",
                Position =
                    new Vector2(game.Graphics.PreferredBackBufferWidth/2 - 300,
                        game.Graphics.PreferredBackBufferHeight/2 - 75),
                OnResize =
                    () =>
                        new Vector2(game.Graphics.PreferredBackBufferWidth/2 - 300,
                            game.Graphics.PreferredBackBufferHeight/2 - 75)
            };

            Loaded = true;
        }

        private void SetButtonVisibilities(int subMenu)
        {
            _creditsButton.ToggleShow(1, HiddenState.Hidden);
            _creditsLabel.ToggleShow(1, HiddenState.Hidden);
            _gameHelpButton.ToggleShow(1, HiddenState.Hidden);
            _gameplayHelpLabel.ToggleShow(1, HiddenState.Hidden);

            switch (subMenu)
            {
                case 0:
                    break;
                case 1:
                    _creditsButton.ToggleShow(1, HiddenState.Visible);
                    _gameHelpButton.ToggleShow(1, HiddenState.Visible);
                    break;
                case 11:
                    _gameplayHelpLabel.ToggleShow(1, HiddenState.Visible);
                    goto case 1;
                case 12:
                    _creditsLabel.ToggleShow(1, HiddenState.Visible);
                    goto case 1;
            }
        }

        public override void Update(GameTime gameTime)
        {
            DoCheck(gameTime, new Point(Game.MState.X, Game.MState.Y), Game.JustPressed());
        }

        public override void Draw(GameTime gameTime)
        {
            Game.SpriteBatch.Begin();

            DoDraw(gameTime, Game.SpriteBatch);

            Game.SpriteBatch.End();
        }

        public override void RecalculateSizes()
        {
            DoResize();
        }
    }
}