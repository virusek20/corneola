﻿using ViruEngine;
using ViruEngine.Console;

namespace Corneola
{
    internal class ForceStateCommand : ConsoleCommand
    {
        public ForceStateCommand(ViruGame game) : base(game)
        {
            Name = "forceState";
            Description = "Forces the game to display a specified state (menu, main)";
            Usage = "(menu, main)";
        }

        public override CommandReturnCode Execute(ConsoleCommandArgs msgArgs)
        {
            if (msgArgs.Arguments.Count != 2) return CommandReturnCode.MissingArguments;

            switch (msgArgs.Arguments[1])
            {
                case "menu":
                    ((CorneolaGame) Game).ActiveState = ((CorneolaGame) Game).MenuState;
                    break;
                case "main":
                    ((CorneolaGame) Game).ActiveState = ((CorneolaGame) Game).MainState;
                    break;
                default:
                    return CommandReturnCode.FailedQuery;
            }

            return CommandReturnCode.Success;
        }
    }
}