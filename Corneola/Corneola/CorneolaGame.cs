using Corneola.GameStates;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine;
using ViruEngine.Graphics.Rendering;

namespace Corneola
{
    public class CorneolaGame : ViruGame
    {
        public MainState MainState;
        public MenuState MenuState;

        public CorneolaGame(string gameName) : base(gameName)
        {
        }

        protected override void Initialize()
        {
            base.Initialize();

            //Graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            //Graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height; 
            Graphics.PreferredBackBufferWidth = 1280;
            Graphics.PreferredBackBufferHeight = 720;
            //Graphics.ToggleFullScreen();
            Graphics.ApplyChanges();

            RenderablePolygon.Initialize(this);

            MenuState = new MenuState();
            MainState = new MainState();

            MainRenderTarget = new RenderTarget2D(GraphicsDevice, Graphics.PreferredBackBufferWidth,
                Graphics.PreferredBackBufferHeight, false, SurfaceFormat.Color, DepthFormat.None, 0,
                RenderTargetUsage.PreserveContents);

            ActiveState = MenuState;
        }
    }
}